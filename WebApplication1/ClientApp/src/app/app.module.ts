import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component'
import { HomeComponent } from './user/home/home.component';
import { AboutComponent } from './user/about/about.component';
import { AuthService } from './AuthGuard/auth.service';
import { AuthGuard, AuthGuardLogin } from './AuthGuard/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', canActivate: [AuthGuardLogin], component: LoginComponent },
      { path: 'home', canActivate: [AuthGuard], component: HomeComponent },
      { path: 'about', canActivate: [AuthGuard], component: AboutComponent },
      { path: '**', redirectTo:'/'}
    ])
  ],
  providers: [ AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
