import { Component, Injectable } from '@angular/core';
import { AuthService } from '../../AuthGuard/auth.service';
import { Userss } from '../../AuthGuard/user.modal';

@Component({
  selector: 'app-content',
  templateUrl: './home.component.html',
})
export class HomeComponent  {
  constructor( private authServie: AuthService) {}

  names: string = "";

  getUser() {
    this.authServie.userss.subscribe( resData => {
      this.names = resData.getUsername;
    })
    return this.names;
  }
}
