import { Component } from '@angular/core';

import { AuthService } from './AuthGuard/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';

  constructor( private authService: AuthService ) {}

  showNav() {
    return this.authService.loginMode();
  }

  onLogout() {
    console.log("logout");
    this.authService.logoutMode();
  }
}
