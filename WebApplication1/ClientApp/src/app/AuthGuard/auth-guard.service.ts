import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, UrlTree, Router } from "../../../node_modules/@angular/router";
import { Injectable } from "../../../node_modules/@angular/core";
import { AuthService } from "./auth.service";
import { AboutComponent } from "../user/about/about.component";
import { Url } from "url";
import { LoginComponent } from "../login/login.component";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private route: Router) {}

    canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree {
        if(this.authService.loginMode()) {
            console.log(this.authService.loginMode());
            return true;
        } else {
            return this.route.parseUrl('');
        }
    }
}

@Injectable({
    providedIn: 'root'
})
export class AuthGuardLogin implements CanActivate {
    constructor(private authService: AuthService, private route: Router) {}

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean | UrlTree {
            if(this.authService.loginMode()) {
                return this.route.parseUrl('home');
            } else {
                return true;
            }
        }
}