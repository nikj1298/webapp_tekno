import { Injectable , Inject } from '@angular/core';
import { HttpClient,  } from '@angular/common/http'; 
import { Router } from '@angular/router';
import { Subject, BehaviorSubject, ReplaySubject } from '../../../node_modules/rxjs';
import { Userss } from './user.modal';
import { tap } from 'rxjs/operators'

interface TokenData {
    Status: boolean;
    Token: string;
    UserName: string;
    Email: string,
}

@Injectable({ providedIn: 'root' })

export class AuthService {
    myAppUrl: string = "";
    isLoginMode = false;

    error = new Subject<number>();
    userss = new ReplaySubject<Userss>(1);

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: Router) {
        this.myAppUrl = baseUrl;
    }

    verifyUser(postData) {
        this.http.post<TokenData>(this.myAppUrl + 'auth', postData)
            .subscribe(responseData => {;
                if(!responseData.Status) {
                    this.error.next(1);
                } else {
                    this.isLoginMode = true;
                    //Storing User Data
                    const user = new Userss( responseData.Token, responseData.UserName, responseData.Email);
                    this.userss.next(user);

                    this.router.navigate(['/home']);
                    this.error.complete();
                }
            },
            errors => {
                this.error.next(2); 
            })
    }

    logoutMode(){
        this.isLoginMode = false;
        this.router.navigate(['/']);
    }

    loginMode(){
        return this.isLoginMode;
    }
}