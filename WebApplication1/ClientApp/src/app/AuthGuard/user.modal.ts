import { Injectable } from "../../../node_modules/@angular/core";

@Injectable({ providedIn: 'root'})
export class Userss {
    constructor( private tokenstring: string, 
        private usern: string,
        private email: string
    ) {}

    get getToken() {
        return this.tokenstring;
    }

    get getUsername() {
        return this.usern;
    }
}