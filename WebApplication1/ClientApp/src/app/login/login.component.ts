import { Component, ViewChild } from '@angular/core';

import { AuthService } from '../AuthGuard/auth.service';

@Component({
  selector: 'app-content',
  templateUrl: './login.component.html'
})

export class LoginComponent {
    errorMessage: string;

    constructor( private authService: AuthService ) {}
  
  @ViewChild('f', { static: false }) loginForm: any;
  @ViewChild('email', { static: false }) emailInput: any;
  @ViewChild('password', { static: false }) passwordInput: any;

  onSubmit(postData) {
    this.errorMessage = '';

    if (!this.loginForm.valid) {
      if (this.emailInput.value == "" || !this.emailInput.valid) {
        this.loginForm.controls['email'].markAsTouched({ onlySelf: true });
      }
      if (this.passwordInput.value == "" || !this.passwordInput.valid) {
        this.loginForm.controls['password'].markAsTouched({ onlySelf: true });
      }
    } else {
        this.authService.verifyUser(postData);
        this.authService.error.subscribe(errors => {
          if(errors == 1){
            this.errorMessage = "Email or Password Incorrect!";
          } else {
            this.errorMessage = "An Error Occured!";
          }
        });
        this.loginForm.reset();
    }
  }
}
