﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public partial class UserData
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string Token { get; set;}
    }
}
