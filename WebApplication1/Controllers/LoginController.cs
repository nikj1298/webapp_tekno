﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    public class LoginController : Controller
    {
        private LoginAuth log = new LoginAuth();

        [HttpPost]
        [Route("auth")]
        public string GetUser([FromBody] Credentials abc)
        {
            if(!string.IsNullOrEmpty(abc.Email) && !string.IsNullOrEmpty(abc.Password))
            {
                return log.Login(abc);
            }
            else
            {
                return "{'Status':'false'}".Replace("'", "\"");
            }
        }
    }
}
