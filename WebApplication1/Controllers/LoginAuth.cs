﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text.Json.Serialization;
using WebApplication1.Controllers;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace WebApplication1.Models
{
    public class LoginAuth
    {
        private readonly Web_TeknoContext db = new Web_TeknoContext();
        private readonly UserData user = new UserData();

        //private readonly IConfiguration _config;

        public string Login(Credentials abc)
        {            
            var user = AuthenticateUser(abc);

            if (user)
            {
                
                var tokenString = GenerateJSONWebToken();
                return $"{{ 'Status': true , 'Token': '{ tokenString }', 'UserName': '{ this.user.Username }', 'Email': '{ this.user.Email }' }}".Replace("'", "\"");
            }

            return "{ 'Status': false }".Replace("'", "\""); ;
        }

        private string GenerateJSONWebToken()
        {
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("MySecretTokenForJwt");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Email, user.Email)
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private bool AuthenticateUser(Credentials abc)
        {
            //Validate the User Credentials
            var myUser = (from u in db.Login
                         where u.Email == abc.Email &&
                               u.Password == CalculateMD5Hash(abc.Password)
                         select u).SingleOrDefault();

            //var myUser = myUs.First();

            if (myUser != null)    //User was found
            {
                user.Email = myUser.Email;
                user.Username = myUser.Username;

                return true;
            }
            return false;
        }

        //MD5 Hash for the 
        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
